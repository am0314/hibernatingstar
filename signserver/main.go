package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/am0314/hibernatingstar/protocal"
)

var orderStatus protocal.MasterOrder = protocal.MasterOrder{
	Main: protocal.MainOrder{
		KeepOn: true,
		// 預設休眠時間為6小時
		SleepcycleH: 6,
	},
	CheckinList: []protocal.StatusReport{},
}

func main() {
	root := gin.Default()
	authRouter := root.Use(func(ctx *gin.Context) {
		sayKey := ctx.GetHeader("say")
		if sayKey != protocal.Hardsecret {
			ctx.String(http.StatusUnauthorized, "")
			ctx.Abort()
		}
	})

	// 取得指令狀態
	authRouter.GET("", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, orderStatus)
	})
	// 設定指令狀態
	authRouter.PUT("", func(ctx *gin.Context) {
		b, err := io.ReadAll(ctx.Request.Body)
		if err != nil {
			log.Println(err.Error())
			return
		}

		tmpStatus := protocal.MainOrder{}
		if err = json.Unmarshal(b, &tmpStatus); err != nil {
			log.Println(err.Error())
			return
		}

		orderStatus.Main = tmpStatus
	})
	// 簽到路由
	authRouter.POST("/checkin", func(ctx *gin.Context) {
		// 客戶端打到這個路由時 紀錄客戶端的IP和地址
		// 紀錄最大長度為50 客戶端Body會帶一個隨機數字字串
		// 這個字串送過來時是加密狀態 要通過解密驗證 才會留下紀錄

		// 紀錄客戶端IP和時間
		orderStatus.CheckinList = append(orderStatus.CheckinList, protocal.StatusReport{
			IPAddr:      ctx.ClientIP(),
			Checkintime: time.Now(),
		})
		// 截斷超過50筆的紀錄
		if len(orderStatus.CheckinList) > 50 {
			orderStatus.CheckinList = orderStatus.CheckinList[1:]
		}
		ctx.JSON(http.StatusOK, gin.H{"message": "checkin success"})
	})

	root.Run(protocal.Portstr)
}
