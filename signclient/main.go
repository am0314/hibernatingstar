package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os/exec"
	"time"

	"gitlab.com/am0314/hibernatingstar/protocal"
)

func main() {
	masterOrder, err := getMainInstructionStatus()
	if err != nil {
		// 可能是網路問題 休眠
		return
	}

	// 不關機
	if masterOrder.Main.KeepOn {
		return
	} else {
		wakeTime := time.Now().Add(
			time.Duration(masterOrder.Main.SleepcycleH) *
				time.Hour,
		).Unix()
		setWakeTime := exec.Command("bash", "-c", "echo 0 > /sys/class/rtc/rtc0/wakealarm") // 清除之前的喚醒時間
		setWakeTime.Run()
		setWakeTime = exec.Command("bash", "-c", fmt.Sprintf("echo %d > /sys/class/rtc/rtc0/wakealarm", wakeTime)) // 設置新的喚醒時間
		setWakeTime.Run()

		// 執行休眠命令
		suspendCmd := exec.Command("systemctl", "suspend")
		err := suspendCmd.Run()
		if err != nil {
			// 休眠失敗
			return
		}
	}
}

func getMainInstructionStatus() (*protocal.MasterOrder, error) {

	{
		req, err := http.NewRequest("POST", protocal.NetAddrCheckin, nil)
		if err != nil {
			return nil, err
		}
		req.Header.Set("say", protocal.Hardsecret)
		_, err = http.DefaultClient.Do(req)
		if err != nil {
			return nil, err
		}
	}

	tmpOrdr := protocal.MasterOrder{}
	{
		req, err := http.NewRequest("GET", protocal.NetAddr, nil)
		if err != nil {
			return nil, err
		}
		req.Header.Set("say", protocal.Hardsecret)
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return nil, err
		}
		// Parse the JSON response into a MasterOrder struct
		buffer, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(buffer, &tmpOrdr)
		if err != nil {
			return nil, err
		}
	}

	return &tmpOrdr, nil // Placeholder
}
