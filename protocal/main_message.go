package protocal

import "time"

const (
	Port    = 4130
	Portstr = ":4130"
	IpAddr  = "34.80.139.127"
	NetAddr = "http://" + IpAddr + Portstr

	NetAddrCheckin = "http://" + IpAddr + Portstr + "/checkin"

	Hardsecret = "4eqf61qe5f1qw65fqw615qfw651qw561"
)

type MasterOrder struct {
	Main        MainOrder
	CheckinList []StatusReport
}

type MainOrder struct {
	KeepOn      bool
	SleepcycleH int64
}

type StatusReport struct {
	IPAddr      string
	Checkintime time.Time
}
